-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 24 mei 2019 om 09:04
-- Serverversie: 10.1.38-MariaDB
-- PHP-versie: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aldofieuw_videotheek_symfony_2`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `exemplaar`
--

CREATE TABLE `exemplaar` (
  `id` int(11) NOT NULL,
  `id_film_id` int(11) NOT NULL,
  `id_gebruiker_id` int(11) DEFAULT NULL,
  `nummer` int(11) NOT NULL,
  `aanwezig` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `exemplaar`
--

INSERT INTO `exemplaar` (`id`, `id_film_id`, `id_gebruiker_id`, `nummer`, `aanwezig`) VALUES
(1, 1, NULL, 5, 0),
(2, 1, NULL, 10, 1),
(3, 1, NULL, 4, 0),
(5, 1, NULL, 1, 0),
(6, 2, NULL, 3, 1),
(8, 1, 1, 2, 1),
(15, 10, NULL, 8, -1),
(18, 4, NULL, 6, 1),
(20, 8, NULL, 9, 1),
(21, 9, NULL, 20, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `titel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lidwoord` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imdb_movie_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `film`
--

INSERT INTO `film` (`id`, `titel`, `lidwoord`, `imdb_movie_id`) VALUES
(1, 'Booksmart', NULL, 'tt1489887'),
(2, 'Welcome to Marwen', NULL, 'tt3289724'),
(3, 'Shawshank Redemption', 'The', 'tt0111161'),
(4, 'Godfather', 'The', 'tt0068646'),
(8, 'Funny Story', NULL, 'tt7422552'),
(9, 'Godfather: Part II', 'The', 'tt0071562'),
(10, 'Godfather: Part III', 'The', 'tt0099674'),
(12, 'Ex Machina', NULL, 'tt0470752'),
(13, 'Film zonder imdb movie id', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruiker`
--

CREATE TABLE `gebruiker` (
  `id` int(11) NOT NULL,
  `naam` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voornaam` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wachtwoord` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefoon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `gebruiker`
--

INSERT INTO `gebruiker` (`id`, `naam`, `voornaam`, `email`, `wachtwoord`, `telefoon`) VALUES
(1, 'Fieuw', 'Aldo', 'aldo.fieuw@gmail.com', 'test', '0494135450'),
(2, 'Tremerie', 'Pol', 'pol@email.com', 'test', '0494123456');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `medewerker`
--

CREATE TABLE `medewerker` (
  `id` int(11) NOT NULL,
  `id_gebruiker_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `medewerker`
--

INSERT INTO `medewerker` (`id`, `id_gebruiker_id`) VALUES
(1, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190522061457', '2019-05-22 06:15:31'),
('20190522063451', '2019-05-22 06:35:10');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `exemplaar`
--
ALTER TABLE `exemplaar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nummer_UNIQUE` (`nummer`),
  ADD KEY `IDX_E176663388E2F8F3` (`id_film_id`),
  ADD KEY `IDX_E1766633BF53D6AD` (`id_gebruiker_id`);

--
-- Indexen voor tabel `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `gebruiker`
--
ALTER TABLE `gebruiker`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `medewerker`
--
ALTER TABLE `medewerker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3888E83EBF53D6AD` (`id_gebruiker_id`);

--
-- Indexen voor tabel `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `exemplaar`
--
ALTER TABLE `exemplaar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT voor een tabel `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT voor een tabel `gebruiker`
--
ALTER TABLE `gebruiker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `medewerker`
--
ALTER TABLE `medewerker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `exemplaar`
--
ALTER TABLE `exemplaar`
  ADD CONSTRAINT `FK_E176663388E2F8F3` FOREIGN KEY (`id_film_id`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `FK_E1766633BF53D6AD` FOREIGN KEY (`id_gebruiker_id`) REFERENCES `gebruiker` (`id`);

--
-- Beperkingen voor tabel `medewerker`
--
ALTER TABLE `medewerker`
  ADD CONSTRAINT `FK_3888E83EBF53D6AD` FOREIGN KEY (`id_gebruiker_id`) REFERENCES `gebruiker` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
