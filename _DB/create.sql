-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema aldofi1q_project_videotheek
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema aldofi1q_project_videotheek
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `aldofi1q_project_videotheek` DEFAULT CHARACTER SET utf8 ;
USE `aldofi1q_project_videotheek` ;

-- -----------------------------------------------------
-- Table `aldofi1q_project_videotheek`.`Film`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aldofi1q_project_videotheek`.`Film` (
  `idFilm` INT NOT NULL AUTO_INCREMENT,
  `titel` VARCHAR(140) NOT NULL,
  `lidwoord` VARCHAR(15) NULL,
  `imdb_movie_id` VARCHAR(45) NULL,
  PRIMARY KEY (`idFilm`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aldofi1q_project_videotheek`.`Exemplaar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aldofi1q_project_videotheek`.`Exemplaar` (
  `idExemplaar` INT NOT NULL AUTO_INCREMENT,
  `nummer` INT NULL,
  `aanwezig` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idExemplaar`),
  UNIQUE INDEX `nummer_UNIQUE` (`nummer` ASC))
ENGINE = InnoDB
COMMENT = '		';


-- -----------------------------------------------------
-- Table `aldofi1q_project_videotheek`.`Film_has_Exemplaar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aldofi1q_project_videotheek`.`Film_has_Exemplaar` (
  `Film_idFilm` INT NOT NULL,
  `Exemplaar_idExemplaar` INT NOT NULL,
  PRIMARY KEY (`Film_idFilm`, `Exemplaar_idExemplaar`),
  INDEX `fk_Film_has_Exemplaar_Exemplaar1_idx` (`Exemplaar_idExemplaar` ASC),
  INDEX `fk_Film_has_Exemplaar_Film_idx` (`Film_idFilm` ASC),
  CONSTRAINT `fk_Film_has_Exemplaar_Film`
    FOREIGN KEY (`Film_idFilm`)
    REFERENCES `aldofi1q_project_videotheek`.`Film` (`idFilm`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Film_has_Exemplaar_Exemplaar1`
    FOREIGN KEY (`Exemplaar_idExemplaar`)
    REFERENCES `aldofi1q_project_videotheek`.`Exemplaar` (`idExemplaar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aldofi1q_project_videotheek`.`Gebruiker`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aldofi1q_project_videotheek`.`Gebruiker` (
  `idGebruiker` INT NOT NULL AUTO_INCREMENT,
  `naam` VARCHAR(45) NOT NULL,
  `voornaam` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `wachtwoord` CHAR(128) NOT NULL,
  `telefoon` VARCHAR(45) NULL,
  PRIMARY KEY (`idGebruiker`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aldofi1q_project_videotheek`.`Gebruiker_has_Exemplaar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aldofi1q_project_videotheek`.`Gebruiker_has_Exemplaar` (
  `Gebruiker_idGebruiker` INT NOT NULL,
  `Exemplaar_idExemplaar` INT NOT NULL,
  PRIMARY KEY (`Gebruiker_idGebruiker`, `Exemplaar_idExemplaar`),
  INDEX `fk_Gebruiker_has_Exemplaar_Exemplaar1_idx` (`Exemplaar_idExemplaar` ASC),
  INDEX `fk_Gebruiker_has_Exemplaar_Gebruiker1_idx` (`Gebruiker_idGebruiker` ASC) ,
  CONSTRAINT `fk_Gebruiker_has_Exemplaar_Gebruiker1`
    FOREIGN KEY (`Gebruiker_idGebruiker`)
    REFERENCES `aldofi1q_project_videotheek`.`Gebruiker` (`idGebruiker`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Gebruiker_has_Exemplaar_Exemplaar1`
    FOREIGN KEY (`Exemplaar_idExemplaar`)
    REFERENCES `aldofi1q_project_videotheek`.`Exemplaar` (`idExemplaar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aldofi1q_project_videotheek`.`Medewerker`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aldofi1q_project_videotheek`.`Medewerker` (
  `Gebruiker_idGebruiker` INT NOT NULL,
  INDEX `fk_Medewerker_Gebruiker1_idx` (`Gebruiker_idGebruiker` ASC),
  CONSTRAINT `fk_Medewerker_Gebruiker1`
    FOREIGN KEY (`Gebruiker_idGebruiker`)
    REFERENCES `aldofi1q_project_videotheek`.`Gebruiker` (`idGebruiker`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `aldofi1q_project_videotheek`.`Gebruiker`
-- -----------------------------------------------------
START TRANSACTION;
USE `aldofi1q_project_videotheek`;
INSERT INTO `aldofi1q_project_videotheek`.`Gebruiker` (`idGebruiker`, `naam`, `voornaam`, `email`, `wachtwoord`, `telefoon`) VALUES (1, 'Fieuw', 'Aldo', 'aldo@getnada.com', 'test', '0494135450');

COMMIT;


-- -----------------------------------------------------
-- Data for table `aldofi1q_project_videotheek`.`Medewerker`
-- -----------------------------------------------------
START TRANSACTION;
USE `aldofi1q_project_videotheek`;
INSERT INTO `aldofi1q_project_videotheek`.`Medewerker` (`Gebruiker_idGebruiker`) VALUES (1);

COMMIT;

